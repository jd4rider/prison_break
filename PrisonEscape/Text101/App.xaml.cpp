﻿//
// App.xaml.cpp
// Implementation of the App class.
//

#include "pch.h"
#include "MainPage.xaml.h"

using namespace Template;

using namespace Platform;
using namespace Windows::ApplicationModel;
using namespace Windows::ApplicationModel::Activation;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Core;
using namespace UnityPlayer;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

/// <summary>
/// Initializes the singleton application object.  This is the first line of authored code
/// executed, and as such is the logical equivalent of main() or WinMain().
/// </summary>
App::App()
{
	m_AppCallbacks = ref new AppCallbacks(false);
	m_AppCallbacks->RenderingStarted += ref new RenderingStartedHandler(this, &App::RemoveSplashScreen);
	InitializeComponent();
}

/// <summary>
/// Invoked when the application is launched normally by the end user.  Other entry points
/// will be used when the application is launched to open a specific file, to display
/// search results, and so forth.
/// </summary>
/// <param name="pArgs">Details about the launch request and process.</param>
void App::OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ pArgs)
{
	m_AppCallbacks->SetAppArguments(pArgs->Arguments);
	// Do not repeat app initialization when already running, just ensure that
	// the window is active
	if (pArgs->PreviousExecutionState == ApplicationExecutionState::Running)
	{
		Window::Current->Activate();
		return;
	}

	if (pArgs->PreviousExecutionState == ApplicationExecutionState::Terminated)
	{
		//TODO: Load state from previously suspended application
	}

	auto mainPage = ref new MainPage(pArgs->SplashScreen);
	Window::Current->Content = mainPage;
	Window::Current->Activate();

	// Setup scripting bridge
	_bridge = ref new WinRTBridge::WinRTBridge();
	m_AppCallbacks->SetBridge(_bridge);
	
	m_AppCallbacks->SetKeyboardTriggerControl(mainPage);
	m_AppCallbacks->SetSwapChainBackgroundPanel(mainPage->GetSwapChainBackgroundPanel());

	auto coreWindow = Window::Current->CoreWindow;

	m_AppCallbacks->SetCoreWindowEvents(coreWindow);

	m_AppCallbacks->InitializeD3DXAML();
}

void App::RemoveSplashScreen()
{
	// This will fail if you change main window class
	// Make sure to adjust accordingly if you do something like this
	MainPage^ page = safe_cast<MainPage^>(Window::Current->Content);
	page->RemoveSplashScreen();
}
