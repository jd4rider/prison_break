﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextController : MonoBehaviour {

	public Text text;
	
	private enum States {
		cell, mirror, sheets_0, lock_0, cell_mirror, sheets_1, lock_1, freedom, 
		corridor_0, corridor_1, corridor_2, corridor_3, stairs_0, stairs_1, stairs_2, floor, closet_door, in_closet, solitary, 
		courtyard,
		};
	private States myState;
	
	// Use this for initialization
	void Start () {
		myState = States.cell;
	}
	
	// Update is called once per frame
	void Update () {
		print (myState);
		if 		(myState == States.cell)		{cell();}
		else if (myState == States.sheets_0)	{sheets_0();}
		else if (myState == States.sheets_1)	{sheets_1();}
		else if (myState == States.lock_0)		{lock_0();}
		else if (myState == States.lock_1)		{lock_1();}
		else if (myState == States.mirror)		{mirror();}
		else if (myState == States.cell_mirror)	{cell_mirror();}
		else if (myState == States.corridor_0)	{corridor_0();}
		else if (myState == States.corridor_1)	{corridor_1();}
		else if (myState == States.corridor_2)	{corridor_2();}
		else if (myState == States.corridor_3)	{corridor_3();}
		else if (myState == States.stairs_0)	{stairs_0();}
		else if (myState == States.stairs_1)	{stairs_1();}
		else if (myState == States.stairs_2)	{stairs_2();}
		else if (myState == States.floor)		{floor();}
		else if (myState == States.closet_door)	{closet_door();}
		else if (myState == States.in_closet)	{in_closet();}
		else if (myState == States.solitary)	{solitary();}
		else if (myState == States.courtyard)	{courtyard();}
	}
		
	#region states handler
	void cell (){
		text.text = "You are in a prison cell, and you want to escape. There are " +
					"some dirty sheets on the bed, a mirror on the wall, and the door " + 
					"is locked from the outside.\n\n" +
					"Press S to view Sheets, M to view Mirror and L to view Lock";
		if 		(Input.GetKeyDown (KeyCode.S))	{myState = States.sheets_0;} 
		else if (Input.GetKeyDown(KeyCode.M)) 	{myState = States.mirror;} 
		else if (Input.GetKeyDown(KeyCode.L)) 	{myState = States.lock_0;}			
	}
	
	void mirror (){
		text.text = "The dirty old mirror on the wall seems loose.\n\n " +
					"Press T to Take the mirror, or R to Return to cell";
		if 		(Input.GetKeyDown (KeyCode.T))	{myState = States.cell_mirror;}
		else if (Input.GetKeyDown(KeyCode.R)) 	{myState = States.cell;}				
	}	

	void cell_mirror (){
		text.text = "You are still in your cell, and you STILL want to escape! There are " +
					"some dirty sheets on the bed, a mark where the mirror was, " +
					"and that pesky door is still there, and firmly locked!\n\n" +
					"Press S to view Sheets or L to view Lock" ;
		if 		(Input.GetKeyDown (KeyCode.S))	{myState = States.sheets_1;}
		else if (Input.GetKeyDown(KeyCode.L)) 	{myState = States.lock_1;}				
	}
				
	void sheets_0 (){
		text.text = "You can't believe you sleep in these things. Surely it's " +
					"time somebody changed them. The pleasures of prison life " + 
					"I guess!\n\n" +
					"Press R to Return to roaming your cell";
		if 		(Input.GetKeyDown (KeyCode.R))	{myState = States.cell;}				
	}

	void sheets_1 (){
		text.text = "Holding a mirror in your hand doesn't make the sheets look " +
					"any better.\n\n" +
					"Press R to Return to roaming your cell";
		if 		(Input.GetKeyDown (KeyCode.R))	{myState = States.cell_mirror;}				
	}
		
	void lock_0 (){
		text.text = "This is one of those button locks. You have no idea what the " +
					"combination is. You wish you could somehow see where the dirty " + 
					"fingerprints were, maybe that would help.\n\n" +
					"Press R to Return to roaming your cell";
		if 		(Input.GetKeyDown (KeyCode.R))	{myState = States.cell;}				
	}
	
	void lock_1 (){
		text.text = "You carefully put the mirror through the bars, and turn it round " +
					"combination is. You wish you could somehow see where the dirty " + 
					"fingerprints were, maybe that would help.\n\n" +
					"Press O to Open, or R to Return to your cell";
		if 		(Input.GetKeyDown (KeyCode.O))	{myState = States.corridor_0;} 
		else if (Input.GetKeyDown (KeyCode.R))	{myState = States.cell_mirror;}				
	}
	
	void corridor_0 (){
		text.text = "You are in a corridor and you notice that you are presented with " +
					"three options.  You could creep on up the Stairs, Search around on " +
					"the floor or slip into the Closet	\n\n " +
					"Press S to try the Stairs, F to search the Floor or C to slip into the Closet";
		if 		(Input.GetKeyDown (KeyCode.S))	{myState = States.stairs_0;}
		else if (Input.GetKeyDown (KeyCode.F))  {myState = States.floor;}
		else if (Input.GetKeyDown (KeyCode.C)) 	{myState = States.closet_door;}
	}
	
	void stairs_0 (){
		text.text = "You sneak up the stairs very quietly and cautiously only to realized " +
					"that there are two guards chatting at the top of the stairs. " +
					"You realize that it will be best to sneak back down the stairs " +	
					"and reasses the situation in the corridor. \n\n " +
					"Press C to Sneak back down the Stairs and return to the corridor"; 
		if 		(Input.GetKeyDown (KeyCode.C))	{myState = States.corridor_0;}
	}	

	void stairs_1 (){
		text.text = "You decide to go for it so you run up the stairs and " +
					"lunge at the first guard with your hairclip raised high! " +
					"After an intense yet brief struggle you are subdued " +	
					"Do you realize the punishment for attempting escape and " +
					"Attempted murder of the guards? \n\n " +
					"Press C to Continue"; 
		if 		(Input.GetKeyDown (KeyCode.C))	{myState = States.solitary;}
	}
	
	void stairs_2 (){
		text.text = "You have decided to make a break for it. There just isn't any time " +
					"to put on the clothes.  It sounds like the guards just walked past " +
					"and they will discover that you aren't in your cell. So, dart up the stairs... \n\n" +	
					"Crap!!!! They are right there.  After a brief struggle you are captured!! \n\n " +
					"Press C to Continue"; 
		if 		(Input.GetKeyDown (KeyCode.C))	{myState = States.solitary;}
	}
			
	void floor (){
		text.text = "You search around on the floor and you don't see anything right off. " +
					"On closer look you see a small glint of light off something metal. " +
					"It's a hairclip! You reach down to pick it up, but you pause because " +	
					"you hear a guard approaching, yet you realize this might be your last chance. \n\n " +
					"Press H to quickly sprint over and grab the hairclip. \n" +
					"Or press R to return to hunker down in the corridor until the gaurd passes."; 
		if 		(Input.GetKeyDown (KeyCode.H))	{myState = States.corridor_1;}
		else if (Input.GetKeyDown (KeyCode.R))	{myState = States.corridor_0;}
	}
	
	void closet_door (){
		text.text = "You notice the closet door and think that maybe if you could just" +
					"slip into that closet there may be some Janitor's clothes that " +
					"you can borrow to slip past the guards unnoticed. " +	
					"You reach up to try the handle on the door of the closet but " +
					"It is locked.  You will have to find something to pick the lock " +
					"to get inside.  If you could only find something.....\n\n " +
					"Press R to return to the corridor to look for something to pick the lock."; 
		if 		(Input.GetKeyDown (KeyCode.R))	{myState = States.corridor_0;}
	}
	
	void corridor_1 (){
		text.text = "So, now that you have the Hairclip in your hand you could sneak up the " +
					"stairs and, using the hairclip as a weapon, take out the guards " +
					"so that you can slip out the main gate into the courtyard " +	
					"You also think that the hairclip may work to Unlock the Closet door " +
					"Time is of the essence! You need to make your choice quickly before " +
					"the guards discover you are no longer in your cell!!! \n\n " +
					"Press S to head up the stairs or P to pick the closet door"; 
		if 		(Input.GetKeyDown (KeyCode.S))	{myState = States.stairs_1;}
		else if (Input.GetKeyDown (KeyCode.P))	{myState = States.in_closet;}
	}
	
	void corridor_2 (){
		text.text = "Time is running out. You open the door quietly and rush over to the stairs " +
					"You realize that you are going to have to make a run for it and hope to find" +
					"that the guards have moved on from the top of the stairs so you dart on up the stairs \n\n " +
					"Press C to Continue"; 
		if 		(Input.GetKeyDown (KeyCode.C))	{myState = States.stairs_2;}
	}

	void corridor_3 (){
		text.text = "Now, dressed in your Janitor's uniform, you walk on out of the closet " +
					"full of purpose and fully confident that this plan will work!!" +
					"Except, these Janitor's clothes are super itchy.  You hope your nut " +	
					"Allergy doesn't kick in! Crap! There's almonds in the pocket. Your pretty much " +
					"A dead man! Soon your Airway will shut down and you won't be able to breath. " + 
					"Should you just press on and hope for the best or Jump " +
					"back in the closet and get out of these Danged clothes \n\n " +
					"Press U to get quickly back in the closet and get Undressed \n" +
					"Or Press S to climb the stairs and pass the guards to the courtyard"; 
		if 		(Input.GetKeyDown (KeyCode.U))	{myState = States.in_closet;}
		else if (Input.GetKeyDown (KeyCode.S))	{myState = States.courtyard;}
	}
		
	void in_closet (){
		text.text = "Now that you are in the closet you have two choices. Either put " +
					"on the Janitor's Uniform you find there and hope the guards are " +
					"fooled by your disguise or return to the corridor and hope to make a run" +	
					"for it.  Remember, time is running out.  The guards could " +
					"find your empty cell any minute!!! \n\n " +
					"Press D to dress up \n "+
					"Or R to Return to corridor and take your chances"; 
		if 		(Input.GetKeyDown (KeyCode.D))	{myState = States.corridor_3;}
		else if (Input.GetKeyDown (KeyCode.R))	{myState = States.corridor_2;}
	}
	
	void solitary (){
		text.text = "You have been caught!!! You are thrown into Solitary Confinement " +
					"for attempting to escape and attempting murder of the guards. " +
					"There's no way out of this one! You'll be lucky if you don't " +	
					"Get life or the death sentence after a stunt like that. \n\n"+
					"GAME OVER!! \n\n " +
					"Press P to Play Again"; 
		if 		(Input.GetKeyDown (KeyCode.P))	{myState = States.cell;}
	}

	void courtyard (){
		text.text = "You made it!!! You are as good as free!! " +
					"Now, to just find a place to get out of these clothes " +
					"Your airway hasn't closed off yet, but you sure are itchy all over!!! " +	
					"You are free!!! \n\n"+
					"YOU WIN!! \n\n " +
					"Press P to Play Again"; 
		if 		(Input.GetKeyDown (KeyCode.P))	{myState = States.cell;}
	}
	#endregion					
}
